const express = require('express');
const router = express.Router();
const { upload, findImage , createPost, likePost, getAllPosts, unLikePost} = require('../controllers/postController.js');
const { validate, validateSchema} = require('../middleware/validation.js');
router.get('/',validate('getAllPosts'),validateSchema,async (req, res) => {
   getAllPosts(req,res);
});

router.post('/new', upload.single('image'),validate('createPost'),validateSchema,async function (req, res) {
  createPost(req,res);
});

router.get('/image/:filename',validate('findImage'),validateSchema,async (req, res) => {
    findImage(req,res);
});


router.post('/like/:postId',validate('like'),validateSchema, async function (req, res) {
  likePost(req,res);
});


router.post('/unLike/:postId',validate('unlike'),validateSchema, async function (req, res) {
  unLikePost(req,res);
});
module.exports = router;
