# MightyGram

The application starts in localhost:5000.
The swagger documentation can be found in localhost:5000/api-docs
For the project i used a local Mongo database witch is great for this kind of 
project since it is highly scalable and has a very flexible schema. 
MongoDB also works very well for javascript objects since it uses json objects to save the data to disk.
The mongo db must be running as a service using the "mongod" command. 
The images are saved in the GridFS storage of MongoDB and the Posts meta data are saved in 
normal MongoDB collections. 
The aplicaion API was tested using the Postman rest client.