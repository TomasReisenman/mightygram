const { GridFsStorage } = require("multer-gridfs-storage");
const url = 'mongodb://localhost:27017/database';
const multer = require('multer');
const mongoose = require("mongoose");
const Grid = require('gridfs-stream');
const Post = require('../models/Post');
const conn = mongoose.createConnection(url);


require('../connection');
let gfs;

conn.once('open', () => {
  gfs = Grid(conn.db, mongoose.mongo);
  gfs.collection('fs');
});

const storage = new GridFsStorage({ url,
 file: (req, file) => {
    if (file.mimetype === 'image/png') {
      return {
        metadata: {
            description: "Good"
        } 
      };
    } else {
      return null;
    }
  }
});

const upload = multer({ storage });

function findImage(req,res){
  gfs.files.findOne({ filename: req.params.filename }, (err, file) => {

    if (!file || file.length === 0) {
      return res.status(404).json({
        err: 'No file exists'
      });
    }

    if (file.contentType === 'image/jpeg' || file.contentType === 'image/png') {

      const readstream = gfs.createReadStream(file.filename);
      readstream.pipe(res);
    } else {
      res.status(404).json({
        err: 'Not an image'
      });
    }
  });
}

async function createPost(req,res){

  if (!req.file) {
    res.status(404).json({error: 'Please provide an image'});
  }

  const newPost = new Post({
      userId : req.body.userId,
      imageId : 0,
      likes: 0,
      description : req.body.description
  });

  newPost.imageId = req.file.filename;

  await newPost.save();
  return res.status(200).json(newPost);

}

async function likePost(req,res){

    const userId = req.body.userId;
    const postFound = await Post.findOne({ _id: req.params.postId });

  if (!postFound) {
    res.status(404).json({error: 'Post not found'});
  }
    const foundValue = postFound.userLikes.find(like => like === userId);
    if (!foundValue ){
      postFound.userLikes.push(userId);
      postFound.likes += 1;
      await postFound.save();
      return res.status(200).json(postFound);
    } else {
      res.status(401).json({ error: 'User already liked post' });
    }
}


async function unLikePost(req,res){

    const userId = req.body.userId;
    const postFound = await Post.findOne({ _id: req.params.postId });

  if (!postFound) {
    res.status(404).json({error: 'Post not found'});
  }
    const foundValue = postFound.userLikes.find(like => like === userId);
    if (!foundValue ){
      res.status(404).json({error: 'User did not have a like for this post'});
    } else {

      postFound.userLikes = postFound.userLikes.filter(like => like !== userId);
      postFound.likes -= 1;
      await postFound.save();
      return res.status(200).json(postFound);
    }
}

async function getAllPosts(req,res){

      const postsPerPage = req.body.postsPerPage;
      const pageNumber = req.body.pageNumber;

    const posts = await Post.find({}).sort({'createdAt': -1}).skip(postsPerPage * pageNumber).limit(postsPerPage);
    return res.status(200).json(posts);
}


module.exports = {
    upload,
    findImage,
    createPost,
    likePost,
    getAllPosts,
    unLikePost
};