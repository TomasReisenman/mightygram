const express = require('express');
const postsRouter = require('./routes/posts');
const swaggerJsDoc = require("swagger-jsdoc");
const swaggerUi = require("swagger-ui-express");
const swaggerDocument = require("./swagger.json"); 
const app = express();
const PORT = process.env.PORT || 5000;

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use('/posts', postsRouter);
app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDocument));
app.get('/', (req, res) => {
  res.send('Welcome to MightyGram'); }
);

app.listen(PORT, () => console.log(`Server started on port ${PORT}`));


module.exports = app;
