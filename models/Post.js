const { Schema, model } = require('mongoose');

const postSchema = new Schema({
    userId: Number,
    description: String,
    imageId: String,
    userLikes:{
        type:[String],
        default: []
    },
    likes: Number
},
{
    timestamps: true
});

module.exports = model('Post', postSchema);