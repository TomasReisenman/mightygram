const { body ,validationResult ,param} = require('express-validator')

function validate(method){

 switch (method) {
    case 'getAllPosts': {
     return [ 
        body('postsPerPage', 'postsPerPage not found').exists().isInt(),
        body('pageNumber', 'pageNumber not found').exists().isInt()
       ]   
    }
    case 'findImage': {
     return [ 
        param('filename', 'filename not found').exists().isString(),
       ]   
    }
    case 'createPost': {
     return [ 
        body('userId', 'userId not found and be a number').exists(),
        body('description', 'description not found').exists(),
       ]   
    }
    case 'like': {
     return [ 
        body('userId', 'userId must be provided and be a number').exists().isInt(),
       ]   
    }
    case 'unlike': {
     return [ 
        body('userId', 'userId must be provided and be a number').exists().isInt(),
       ]   
    }
  }
}

async function validateSchema(req, res, next) {

  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(422).json({ errors: errors.array() });
  }
  next();

}

module.exports = {
    validate,
    validateSchema
};